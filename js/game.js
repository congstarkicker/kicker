// Create the state that will contain the whole game
var soccerfield;
var width = 970;
var height = 635;
var minballSpeed = 300
var gapSize = 200;

var mainState = {
  preload: function() {
    game.load.image('player1figure', 'Images/player.png');
    game.load.image('player2figure', 'Images/player2.png');
    game.load.image('ball', 'Images/ball.png');
    game.load.image('soccerfield', 'Images/soccer_field.png');
    game.load.image('goalLeft', 'Images/goal.png');
    game.load.image('goalRight', 'Images/goal.png');
    
    //goalCorners
    game.load.image('goalCornerUpperLeft', 'Images/doorVosten.png');
    
  },
  create: function() {
    // Set the background color to blue
    game.stage.backgroundColor = '#4caf50';

    // Start the Arcade physics system (for movements and collisions)
    game.physics.startSystem(Phaser.Physics.ARCADE);

    soccerfield = game.add.tileSprite(10, 10, width, height, 'soccerfield');
    soccerfield.fixedToCamera = true;

    // Add the physics engine to all the game objetcs
    game.world.enableBody = true;

    // Create the left/right arrow keys
    this.player2Up = game.input.keyboard.addKey(Phaser.Keyboard.O);
    this.player2Down = game.input.keyboard.addKey(Phaser.Keyboard.L);
  
    this.player2Row2Up = game.input.keyboard.addKey(Phaser.Keyboard.I);
    this.player2Row2Down = game.input.keyboard.addKey(Phaser.Keyboard.K);

    this.player1Up = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.player1Down = game.input.keyboard.addKey(Phaser.Keyboard.S);
  
    this.player1Row2Up = game.input.keyboard.addKey(Phaser.Keyboard.E);
    this.player1Row2Down = game.input.keyboard.addKey(Phaser.Keyboard.D);

    this.player1figure1 = game.add.sprite(200, 200, 'player1figure');
    this.player1figure2 = game.add.sprite(200, 200 + gapSize, 'player1figure');
    this.player1figure1.scale.setTo(0.2, 0.2);
    this.player1figure2.scale.setTo(0.2, 0.2);
  
    this.player1figure3 = game.add.sprite(400, 300, 'player1figure');
    //this.player1figure4 = game.add.sprite(400, 500 + gapSize, 'player1figure');
    this.player1figure3.scale.setTo(0.2, 0.2);
    //this.player1figure4.scale.setTo(0.2, 0.2);

    this.player2figure1 = game.add.sprite(width - 200, 200, 'player2figure');
    this.player2figure2 = game.add.sprite(width - 200, 200 + gapSize, 'player2figure');
    this.player2figure1.scale.setTo(0.2, 0.2);
    this.player2figure2.scale.setTo(0.2, 0.2);
  
    this.player2figure3 = game.add.sprite(width - 400, 300, 'player2figure');
    //this.player2figure4 = game.add.sprite(width - 400, 500 + gapSize, 'player2figure');
    this.player2figure3.scale.setTo(0.2, 0.2);
    //this.player2figure4.scale.setTo(0.2, 0.2);

    this.player1 = game.add.physicsGroup();
    this.player1.enableBody = true;
    this.player1.add(this.player1figure1);
    this.player1.add(this.player1figure2);
  
    this.player1Row2 = game.add.physicsGroup();
    this.player1Row2.enableBody = true;
    this.player1Row2.add(this.player1figure3);
    //this.player1Row2.add(this.player1figure4);

    this.player2 = game.add.physicsGroup();
    this.player2.enableBody = true;
    this.player2.add(this.player2figure1);
    this.player2.add(this.player2figure2);
  
    this.player2Row2 = game.add.physicsGroup();
    this.player2Row2.enableBody = true;
    this.player2Row2.add(this.player2figure3);
    //this.player2Row2.add(this.player2figure4);

    this.player1.setAll('body.immovable', true);
    this.player1Row2.setAll('body.immovable', true);

    this.player2.setAll('body.immovable', true);
    this.player2Row2.setAll('body.immovable', true);
    //this.player1.callAll('scale.setTo', '', 0.2, 0.2);

    // Add the ball
    this.ball = game.add.sprite(496, 330, 'ball');

    // Give the ball some initial speed
    this.ball.body.velocity.x = minballSpeed;
    this.ball.body.velocity.y = minballSpeed;

    // Make sure the ball will bounce when hitting something
    this.ball.body.bounce.setTo(1);
    this.ball.scale.setTo(0.25, 0.25);

    this.ball.body.collideWorldBounds = true;

    this.player1.setAll('body.collideWorldBounds', true);
    this.player1Row2.setAll('body.collideWorldBounds', true);
    this.player2.setAll('body.collideWorldBounds', true);
    this.player2Row2.setAll('body.collideWorldBounds', true);

    //add goals
    this.goalLeft = game.add.sprite(15, height/2-45, 'goalLeft');

    this.goalRight = game.add.sprite(width-15, height/2-45, 'goalRight');

    this.scorePlayer1 = 0;
    this.scorePlayer2 = 0;
    
    //goalCorner
    this.goalCornerUpperLeft = game.add.sprite(11,height/2-50, 'goalCornerUpperLeft');
    this.goalCornerDownLeft = game.add.sprite(11,height/2+60, 'goalCornerUpperLeft');
    this.goalCornerUpperRight = game.add.sprite(950,height/2-50, 'goalCornerUpperLeft');
    this.goalCornerDownRight = game.add.sprite(950,height/2+60, 'goalCornerUpperLeft');
    
    //create physic group
    this.goalCorners = game.add.physicsGroup();
    this.goalCorners.enableBody = true;
    
    //add every goalCorner to the group
    this.goalCorners.add(this.goalCornerUpperLeft);
    this.goalCorners.add(this.goalCornerDownLeft);
    this.goalCorners.add(this.goalCornerUpperRight);
    this.goalCorners.add(this.goalCornerDownRight);

    this.goalCorners.setAll('body.immovable', true);
    this.goalCorners.setAll('body.moves', false);
  },

  update: function() {
    this.ball.anchor.setTo(0.5, 0.5);
    this.ball.rotation += 0.1;

    this.updatePlayerPosition(this.player1, this.player1Up, this.player1Down);
    this.updatePlayerPosition(this.player1Row2, this.player1Row2Up, this.player1Row2Down);
    this.updatePlayerPosition(this.player2, this.player2Up, this.player2Down);
    this.updatePlayerPosition(this.player2Row2, this.player2Row2Up, this.player2Row2Down);

    if (this.ball.body.velocity.x > minballSpeed) {
      this.ball.body.velocity.x -= (this.ball.body.velocity.x - 500) / 2;
    }

    if (this.ball.body.velocity.y > minballSpeed) {
      this.ball.body.velocity.y -= (this.ball.body.velocity.y - 500) / 2;
    }

    game.physics.arcade.collide(this.player1, this.ball, this.hit);
  game.physics.arcade.collide(this.player1Row2, this.ball, this.hit);
    game.physics.arcade.collide(this.player2, this.ball, this.hit);
  game.physics.arcade.collide(this.player2Row2, this.ball);
  game.physics.arcade.collide(this.goalCorners, this.ball);

    game.physics.arcade.overlap(this.ball, this.goalLeft, function() {
      setBallToStart(this.ball);
      this.scorePlayer2 += 1;
      document.getElementById('goals-team-two').innerHTML = this.scorePlayer2;
      game.time.events.add(Phaser.Timer.SECOND, startBall, this);
    }, null, this);

    game.physics.arcade.overlap(this.ball, this.goalRight, function() {
      setBallToStart(this.ball);
      this.scorePlayer1 += 1;
      document.getElementById('goals-team-one').innerHTML = this.scorePlayer1;
      game.time.events.add(Phaser.Timer.SECOND, startBall, this);
    }, null, this);

    if (this.scorePlayer2 == 10|| this.scorePlayer1  == 10) {
      this.scorePlayer2 = 0;
      this.scorePlayer1 = 0;
      document.getElementById('goals-team-one').innerHTML = this.scorePlayer1;
      document.getElementById('goals-team-two').innerHTML = this.scorePlayer2;
      game.state.restart('main');
    }

  },

  updatePlayerPosition : function(player, upKey, downKey){
    var amountChildren = player.children.length-1;
    for (var i = 0 in player.children) {
      if (upKey.isDown && player.children[0].position.y > 0) {

          player.children[i].body.velocity.y = -700;
      } else if (downKey.isDown && player.children[amountChildren].position.y < (height - player.children[amountChildren].height)) {

          player.children[i].body.velocity.y = 700;
      } else {
          player.children[i].body.velocity.y = 0;
      }
      if (player.children[amountChildren].position.y >= (height - player.children[amountChildren].height)) {
          player.children[i].position.y = player.children[0].position.y + gapSize * i; 
      }

      if (player.children[0].position.y <= 0) {
          player.children[i].position.y = player.children[0].position.y + gapSize * i; 
      }
    }
  },

  hit : function(ball, player) {
    ball.body.velocity.x = 1.2 * ball.body.velocity.x;
    ball.body.velocity.y = 1.2 * ball.body.velocity.y;
  }

};

function setBallToStart(ball) {
  ball.x = 496;
  ball.y = 330;
  ball.body.velocity.x = 0;
  ball.body.velocity.y = 0;
}

function startBall() {
  this.ball.body.velocity.x = game.rnd.integerInRange(-1,1);
  if (this.ball.body.velocity.x >0) {
    this.ball.body.velocity.x = minballSpeed;
  }
  else {
    this.ball.body.velocity.x = minballSpeed*-1;
  }
  // this.ball.body.velocity.x = minballSpeed*preValue;
  this.ball.body.velocity.y = game.rnd.integerInRange(-300, 300); 
}

// Initialize the game and start our state
var game = new Phaser.Game(width + 20, height + 20, Phaser.CANVAS,
  'soccer-table');
//game.stage.canvas.id = "soccer-table";
game.state.add('main', mainState);
game.state.start('main');
